# catalejo-lua

Este proyecto hace referencia del backend para la aplicación Catalejo Editor, el backend está
basado en luajls para las operaciones de manipulación de archivos. Para el control por serial,
se ajustó la librería correspondiente. El backend se escribe en Lua.

Para el desarrollo de esta aplicación realice los pasos que se encuentran en el `./Makefile`.
