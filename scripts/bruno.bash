#!/bin/bash

APP=bruno
VERSION=1.13.1
SCRIPT_PATH=$(pwd)
BUILDER_PATH=$SCRIPT_PATH/build-apps
DIR_APP=$BUILDER_PATH/$APP
BIN=$SCRIPT_PATH/bin
LIB=$SCRIPT_PATH/lib
DOWNLOAD_APP="https://github.com/usebruno/bruno/releases/download/v$VERSION/bruno_${VERSION}_x86_64_linux.AppImage"

install() {
	rm -rf $DIR_APP
	mkdir -p $DIR_APP
	cd $DIR_APP
	wget -O $APP $DOWNLOAD_APP
	chmod +x $APP
}

help() {
	echo "install"
}

if [[ -v 1 ]]; then
	$1
else
	help
fi
