#!/bin/bash

APP=lua
VERSION=3.11.0
SCRIPT_PATH=$(pwd)
BUILDER_PATH=$SCRIPT_PATH/build-apps
DIR_APP=$BUILDER_PATH/lua-$VERSION
BIN=$SCRIPT_PATH/bin
LIB=$SCRIPT_PATH/lib
DOWNLOAD_APP="wget https://luarocks.org/releases/luarocks-$VERSION.tar.gz"
DOWNLOAD_APP="https://www.lua.org/ftp/lua-$VERSION.tar.gz"

download() {
	mkdir -p $BUILDER_PATH
	cd $BUILDER_PATH
	rm -rf $DIR_APP
	curl -L -R -O $DOWNLOAD_APP
	tar xzf $APP-$VERSION.tar.gz
}

install() {
	wget https://luarocks.org/releases/luarocks-3.11.0.tar.gz
	tar zxpf luarocks-3.11.0.tar.gz
	cd luarocks-3.11.0
	./configure && make && sudo make install
	# sudo luarocks install luasocket
	# lua
}

if [[ -v 1 ]]; then
	$1
fi
