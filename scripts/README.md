# Scripts to download and install tools

* `./bruno.bash`: Instalación de cliente para probar el backend en las solicitudes API REST
* `./lua.bash`: Descarga e instalación del interprete lua, quepor ahora no está siendo requerido en este desarrollo, en vista que luajls ya lo tiene incorporado.
* `./luajls.bash`: Herramienta principal para el backen con diferentes múdulos, entre ellos, se usa: serial, tcp, http, file.
* `./luarocks.bash`: Esta herramienta que es el gestor de paquetes de lua, aún no está siendo __usado__.


**Nota**: Los script en uso para este desarrollo (es decir, que no están en exploración) son, bruno y luajls.

