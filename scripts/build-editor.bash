#!/bin/bash

SCRIPT_PATH=$(pwd)
BUILDER_PATH=$SCRIPT_PATH/build-apps

check() {
  if [[ $? -ne 0 ]]; then
    echo "failed"
    exit $?
  fi
}

# Descarga el frontend de catalejo editor basado en vue
install_frontend() {
  echo "##> Instalación de catalejo editor vue (frontend)"
  mkdir $BUILDER_PATH
  cd $BUILDER_PATH
  rm -rf catalejo-vue3-desktop-v2
  mkdir -p catalejo-vue3-desktop-v2
  cd catalejo-vue3-desktop-v2
  npm pack @johnnycubides/catalejo-vue3-desktop-v2
  mv *.tgz catalejo-vue3-desktop-v2.tgz
  tar xvf catalejo-vue3-desktop-v2.tgz
  cd package/dist
  cp -var * $SCRIPT_PATH/app/ui-app
}

# Montaje de archivos lua para el backend
install_backend() {
  cd $SCRIPT_PATH
  echo "##> Cargando los scripts de Lua para el backende de la aplicación"
  mkdir -p ./app/ui-app/plugins
  cp -var ./src/init.lua ./src/lua ./app/
  cp -var ./src/ui-app/plugins/plugins.lua ./app/ui-app/plugins/
  echo "##> Ajustando configuraciones"
  sed -i '3,3 s/.*/local dirApp = ";..\/app"/' ./app/init.lua
}

# esta función remueve el directorio app que contiene la construcción
# de la aplicación
remove_app() {
  cd $SCRIPT_PATH
  echo "##> Remover directorio app"
  rm -rf app
}

#####################
## MAKE DIST LINUX
#####################
dist_linux() {
  cd $SCRIPT_PATH
  DIST_NAME=catalejo-linux
  DIST=dist/$DIST_NAME
  rm -rf $DIST
  mkdir -p $DIST/work
  touch $DIST/work/README.md
  # 1. cp bin que contiene luajls, lua y lua-libserialport
  cp -var ./bin $DIST
  # 2. cp app a distribución, la app contiene el backend y el frontend
  cp -var ./app $DIST
  # 3 Agregar script de instalación
  cp -var ./scripts/desktop-linux-install/* $DIST
  # 4 Crear enlace simbólico para plugins
  cd $DIST
  # ln -sr app/ui-app/plugins ./ # TODO: cambiar o quitar, por ahora está comentado
  # 5 Empaquetar
  cd ../ && tar cvf $DIST_NAME.tar.gz $DIST_NAME
}

#####################
## MAKE DIST WINDOWS
#####################
dist_w64() {
  cd $SCRIPT_PATH
  DIST_NAME=catalejo-windows
  DIST=dist/$DIST_NAME
  rm -rf $DIST
  mkdir -p $DIST/bin         # Crear directorio para binario
  mkdir -p $DIST/work        # Crear directorio de work
  touch $DIST/work/README.md # Agregar un readme
  # 1. Descomprimir luajls-w64 en bin y copiar libserial.dll a bin
  unzip $BUILDER_PATH/luajls-w64.zip -d $DIST/bin/
  cp -var $BUILDER_PATH/libserial.dll $BUILDER_PATH/libserialport-0.dll $DIST/bin/
  # 2. cp app a distribución, la app contiene el backend y el frontend
  cp -var ./app $DIST
  # 3 Agregar script de instalación
  cp -var ./scripts/desktop-w64-install/* $DIST
  # 4 Crear enlace simbólico para plugins
  cd $DIST
  # ln -sr app/ui-app/plugins ./ # TODO: por ahora está comentado
  # 5 Empaquetar distribución
  cd ../ && zip -r $DIST_NAME.zip $DIST_NAME
}

#####################
## MAKE DIST ANDROID
#####################
dist_android() {
  cd $SCRIPT_PATH
  DIST_NAME=catalejo_android
  DIST=dist/$DIST_NAME
  rm -rf $DIST
  mkdir -p $DIST/
  # Copiar el contenido para distribución exclusiva de android
  cp -var ./app/ui-app/* $DIST
  check
  cd $DIST/
  # Ajustar ruta de directorios para los plugins
  sed -i 's/app\/ui-app\///g' plugins/plugins.js
  check
  zip -r $DIST_NAME.zip *
  # Empaquetar en .zip y presentar
  mv $DIST_NAME.zip ../
  check
}

help() {
  echo ""
}

if [[ -v 1 ]]; then
  $1
else
  help
fi
