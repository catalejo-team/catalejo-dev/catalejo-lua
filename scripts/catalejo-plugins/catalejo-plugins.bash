#!/bin/bash
# La instalación de plugins se realiza en el directorio app/ui-app/plugins

##########################################################################
##
##########################################################################

arrayplugins=($2)

search_string() {
  for thisPlugin in "${arrayplugins[@]}"; do
    if [[ "$thisPlugin" == "$1" ]]; then
      echo "$thisPlugin"
      return 0
    fi
  done
  return 1
}

# Ejecutar función
search_string "nodemcu"
# retorno de la función con $?
plg_nodemcu=$?

search_string "micropython-esp32"
plg_micropython_esp32=$?

# SCRIPT_PATH lugar desde donde se lanza el script
SCRIPT_PATH=$(pwd)
BUILDER_PATH=$SCRIPT_PATH/build-apps

check() {
  if [[ $? -ne 0 ]]; then
    echo "failed"
    exit $?
  fi
}

##########################################################################
##  DESCARGA DE PLUGINS
##########################################################################

pack_download() {
  PACK=$1
  NPM_PACK=$2
  echo "##> Inicio de proceso de instalación de plugin $PACK"
  mkdir $BUILDER_PATH
  cd $BUILDER_PATH
  rm -rf $PACK
  mkdir -p $PACK
  cd $PACK
  npm pack $NPM_PACK
  check
  mv *.tgz $PACK.tgz
  check
  tar xvf $PACK.tgz
  check
  echo "##> Proceso de descargar del plugin $PACK terminado existosamente"
}

micropython_esp32_download() {
  PACK=micropython-esp32-plugin-catalejo-editor
  NPM_PACK=@johnnycubides/$PACK
  pack_download $PACK $NPM_PACK
}

nodemcu_download() {
  PACK=nodemcu-plugin-catalejo-editor
  NPM_PACK=@johnnycubides/$PACK
  pack_download $PACK $NPM_PACK
}

plugins_download() {
  # Evaluar si plg_nodemcu = 0, es decir true
  if [ $plg_nodemcu -eq 0 ]; then
    nodemcu_download
  fi
  if [ $plg_micropython_esp32 -eq 0 ]; then
    micropython_esp32_download
  fi
  echo "##> Finaliza el proceso de plugins_download"
}

##########################################################################
##  INSTALACIÓN DE PLUGINS
##########################################################################

plugin_install() {
  PACK=$1
  NAME_PLUGIN=$2
  rm -rf $SCRIPT_PATH/app/ui-app/plugins/$NAME_PLUGIN
  cp -var $BUILDER_PATH/$PACK/package/$NAME_PLUGIN $SCRIPT_PATH/app/ui-app/plugins/
  check
  echo "##> Finalización de copiado de plugin: $NAME_PLUGIN"
}

nodemcu_plugins_install() {
  plugin_install nodemcu-plugin-catalejo-editor nodemcu
}

micropython_esp32_plugins_install() {
  plugin_install micropython-esp32-plugin-catalejo-editor micropython-esp32
}

plugins_install() {
  if [ $plg_nodemcu -eq 0 ]; then
    nodemcu_plugins_install
  fi
  if [ $plg_micropython_esp32 -eq 0 ]; then
    micropython_esp32_plugins_install
  fi
  echo "## Finaliza el proceso de plugins_install"
}

##########################################################################
##  CONFIGURACIÓN DE PLUGINS JS
##########################################################################
# Variables para la configuración tanto de LUA como JS
SRC_SCRIPT_PLUGINS=$SCRIPT_PATH/scripts/catalejo-plugins
TARGET_PLUGINS_CONFIG=$SCRIPT_PATH/app/ui-app/plugins

plugin_config_js() {
  NAME_PLUGIN=$1
  # Agregar la información para módulo de micropython-esp32
  cat $SRC_SCRIPT_PLUGINS/${NAME_PLUGIN}.js >>$TARGET_PLUGINS_CONFIG/plugins.js
  check
  echo "##> Finalización de CONFIGURACIÓN JS de plugin: $NAME_PLUGIN"
}

start_plugins_js() {
  # Inicia el archivo plugins.js
  cat $SRC_SCRIPT_PLUGINS/plugins_start.js >$TARGET_PLUGINS_CONFIG/plugins.js
  check
}
end_plugins_js() {
  # Finalizar el archico plugins.js
  cat $SRC_SCRIPT_PLUGINS/plugins_end.js >>$TARGET_PLUGINS_CONFIG/plugins.js
  check
  echo "##> Finalización del proceso config_plugins_js"
}

config_plugins_js() {
  start_plugins_js
  if [ $plg_nodemcu -eq 0 ]; then
    plugin_config_js nodemcu
  fi
  if [ $plg_micropython_esp32 -eq 0 ]; then
    plugin_config_js micropython-esp32
  fi
  end_plugins_js
}

##########################################################################
##  CONFIGURACIÓN DE PLUGINS LUA
##########################################################################
start_plugins_lua() {
  # Inicia el archivo plugins.js
  cat $SRC_SCRIPT_PLUGINS/plugins_start.lua >$TARGET_PLUGINS_CONFIG/plugins.lua
  check
}
end_plugins_lua() {
  # Finalizar el archico plugins.js
  cat $SRC_SCRIPT_PLUGINS/plugins_end.lua >>$TARGET_PLUGINS_CONFIG/plugins.lua
  check
  echo "##> Finalización del proceso config_plugins_lua"
}

plugin_config_lua() {
  NAME_PLUGIN=$1
  cat $SRC_SCRIPT_PLUGINS/${NAME_PLUGIN}.lua >>$TARGET_PLUGINS_CONFIG/plugins.lua
  check
  echo "##> Finalización de CONFIGURACIÓN LUA de plugin: $NAME_PLUGIN"
}

config_plugins_lua() {
  start_plugins_lua
  if [ $plg_nodemcu -eq 0 ]; then
    plugin_config_lua nodemcu
  fi
  if [ $plg_micropython_esp32 -eq 0 ]; then
    plugin_config_lua micropython-esp32
  fi
  end_plugins_lua
}
##########################################################################

help() {
  echo "micropython_esp32_download"
  echo "micropython_esp32_install"
  echo "config_plugins_js"
}

if [[ -v 1 ]]; then
  $1
else
  help
fi
