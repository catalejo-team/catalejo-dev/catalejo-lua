#!/bin/bash
# Scripts de descarga e instalación de luaclibs.
# Para linux se descarga e instala desde archivos fuentes,
# Con respecto a Windows, al no ser posible una crosscompilación, se debe hacer uso
# de el compilado del autor.

APP=luaclibs
SCRIPT_PATH=$(pwd)
COMMIT=10bff44334192da90d0582743a42a3b54da8632e
BUILDER_PATH=$SCRIPT_PATH/build-apps
DIR_APP=$BUILDER_PATH/
BIN=$SCRIPT_PATH/bin
LUA_LIBSERIALPORT_V=1.0

# Verificar e instalar las dependecias para la construcción de luaclibs.
# Estas dependencias son requeridas para la construcción desde linux, para
# windows no es posible hacer crosscompilación.
# Verificar e instalar las dependecias para la COMPILACIÓN de luaclibs en LINUX
# 1. Si FALLA la instalación de estas dependencias, deberá hacer uso de una compilación a través
# de un contenedor de docker con podman.
dependencies-linux() {
  sudo apt update
  sudo apt install \
    libbluetooth-dev \
    libgtk-3-dev \
    libwebkit2gtk-4.0-dev \
    -y
  check
  # La dependecias libwebkit2gtk-4.0-dev en algunos sistemas ya ha sido cambiada por la v4.1
  # Mientras que no se haga una migración del código fuente de luaclibs/webview, se deberá
  # compilar con DOCKER.
}

# Descargar luaclibs (archivos fuentes) en el directorio donde se construyen
# las apps para el proyecto (build-apps)
download() {
  mkdir -p $BUILDER_PATH
  cd $BUILDER_PATH
  git clone git@github.com:javalikescript/luaclibs.git $APP
}

# Poner la versión de luaclibs en el commit de los binarios para el backend de
# catalejo editor junto a los demás submódulos (dependecias).
checkout() {
  cd $BUILDER_PATH/$APP
  git checkout $COMMIT
  git submodule update --init --recursive
}

# En LINUX, Limpiar configuraciones anteriores, CONSTRUIR los cores de los módulos (make),
# CONFIGURAR los todos los módulos creados (make configure and make all
build-linux() {
  cd $BUILDER_PATH/$APP
  rm -rf dist
  make clean
  make
  make configure
  make all
}

# En LINUX, Se crea el directorio de distribución dist/ que contiene los binarios
# y el contenido de dist/ es alojado en el directorio bin/
install-linux() {
  mkdir -p $BIN
  cd $BUILDER_PATH/$APP
  make dist
  cd dist
  cp -var * $BIN
  lua-libserialport-install-linux
}

# Instalacion de librería para el manejo del puerto serial
lua-libserialport-install-linux() {
  echo "Instalacion de librería lua-libserialport"
  cd $BUILDER_PATH/
  wget -O libserial.zip "https://github.com/johnnycubides/lua-libserialport/releases/download/$LUA_LIBSERIALPORT_V/libserial.zip"
  unzip libserial.zip -d $BIN
}

## Descarga de luajls y libserial para windows 64 bits
VERSION_LUAJLS=20240407
download-w64() {
  echo "##> Descargar luajls para windows 64 bits"
  cd $BUILDER_PATH/
  wget -O luajls-w64.zip "https://github.com/javalikescript/luajls/releases/download/0.8.2/luajls-5.4-x86_64-windows.$VERSION_LUAJLS.zip"
  lua-libserialport-download-w64
  ls
}
lua-libserialport-download-w64() {
  echo "##> Descargar libserialport.dll para windows 64 bits"
  cd $BUILDER_PATH/
  wget -O libserial.dll "https://github.com/johnnycubides/lua-libserialport/releases/download/$LUA_LIBSERIALPORT_V/libserial.dll"
  wget -O libserialport-0.dll "https://github.com/johnnycubides/lua-libserialport/releases/download/$LUA_LIBSERIALPORT_V/libserialport-0.dll"
  ls
}

if [[ -v 1 ]]; then
  $1
fi

check() {
  if [[ $? -ne 0 ]]; then
    echo "failed"
    exit $?
  fi
}
