-- portname: /dev/ttyUSB0
-- platform: nodemcu
-- dtr: 0
-- rts: 0

azul = (4)
gpio.mode(azul, gpio.OUTPUT)
for count = 1, 10 do
  tmr.delay(0.2*1000000)
  gpio.write(azul, (gpio.read(azul) == 1 and gpio.LOW or gpio.HIGH))
  tmr.delay(0.2*1000000)
  gpio.write(azul, (gpio.read(azul) == 1 and gpio.LOW or gpio.HIGH))
end