local httpServer = require("serverHttp")
local HttpHandler = require("jls.net.http.HttpHandler")

-- local ProcessBuilder = require("jls.lang.ProcessBuilder")
-- local Pipe = require("jls.io.Pipe")
-- local Promise = require("jls.lang.Promise")

-- local pluginCommands = function(command)
-- 	return Promise:new(function(resolve, reject)
-- 		local pb = ProcessBuilder:new(command)
-- 		local p = Pipe:new()
-- 		pb:redirectOutput(p)
-- 		pb:start(function(_)
-- 			p:readStart(function(_, data)
-- 				if data then
-- 					print("data: ")
-- 					resolve(data)
-- 				else
-- 					print("fail")
-- 					reject("error")
-- 				end
-- 			end)
-- 		end)
-- 	end)
-- end
--
-- local handler = HttpHandler:new(function(self, exchange)
-- 	return Promise:new(function(resolve)
-- 		pluginCommands({ "ls", "-ltr" }):next(function(res)
-- 			local response = exchange:getResponse()
-- 			print(res)
-- 			response:setBody(res)
-- 			resolve()
-- 		end)
-- 	end)
-- end)

-- tipo de respuestas enviadas al cliente
local STATUS = {
	OK = 0,
	FAILED = 1,
}

local function headersHandler(exchange)
	-- local data2str = json.encode(data)
	local response = exchange:getResponse()
	-- return response:setBody("This is a respond")
	response:setHeader("Content-Type", " application/json")
	response:setHeader("Access-Control-Allow-Origin", "*")
	response:setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS, DELETE")
	response:setHeader("Access-Control-Allow-Headers", "Content-Type")
	return {}
end

-- Name funcion
local command1Handler = HttpHandler:new(function(_, exchange)
	if exchange:getRequestMethod() == "OPTIONS" then
		return headersHandler(exchange)
	else
		return exchange:getRequest():json():next(function(res)
			local hello = res.hello
			headersHandler(exchange)
			return { status = STATUS.OK, data = hello }
		end)
	end
end)

-- Name funcion
local command2Handler = HttpHandler:new(function(_, exchange)
	if exchange:getRequestMethod() == "OPTIONS" then
		return headersHandler(exchange)
	else
		return exchange:getRequest():json():next(function(res)
			local hello = res.hello
			headersHandler(exchange)
			return { status = STATUS.OK, data = hello }
		end)
	end
end)

httpServer.service():createContext(
	"/pluginName/(.*)",
	HttpHandler.router({
		["command1?method=PUT,OPTIONS"] = function(exchange)
			return command1Handler:handle(exchange)
		end,
		["command2?method=PUT,OPTIONS"] = function(exchange)
			return command2Handler:handle(exchange)
		end,
	})
)

print("START default plugin")
