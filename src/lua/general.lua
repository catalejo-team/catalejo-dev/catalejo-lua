local general = {}

-- Función recursiva para imprimir una tabla
function general.printTable(tabla, nivel)
	nivel = nivel or 0 -- Nivel de indentación inicial

	-- Recorrer los elementos de la tabla
	for clave, valor in pairs(tabla) do
		-- Imprimir indentación
		for i = 1, nivel do
			io.write("  ")
		end

		-- Imprimir clave y valor
		io.write(tostring(clave) .. ": ")

		if type(valor) == "table" then
			-- Si el valor es otra tabla, imprimir recursivamente
			io.write("\n")
			general.printTable(valor, nivel + 1)
		else
			-- Si el valor no es una tabla, imprimir directamente
			io.write(tostring(valor) .. "\n")
		end
	end
end

function general.join_path_and_file(directory, filename)
	-- Determinar el separador de directorios
	local separator
	if string.find(directory, "/") then
		-- Formato Unix
		separator = "/"
	elseif string.find(directory, "\\") then
		-- Formato Windows
		separator = "\\"
	else
		-- Asumir Unix por defecto
		separator = "/"
	end

	-- Verificar si el path termina con el separador
	if string.sub(directory, -1) ~= separator then
		directory = directory .. separator
	end

	-- Concatenar el path y el nombre del archivo
	local full_path = directory .. filename
	return full_path
end

function general.getHeaderName(cadena, name)
	-- Crear una expresión regular para encontrar el campo específico, permitiendo caracteres especiales y secuencias de escape
	local patron = name .. '="([^"]-)"'
	-- Buscar el valor usando la expresión regular
	local valor = string.match(cadena, patron)
	-- Si se encuentra el valor, reemplazar las secuencias de escape
	if valor then
		valor = valor:gsub('\\"', '"') -- Reemplazar \" con "
		valor = valor:gsub("\\\\", "\\") -- Reemplazar \\ con \
	end
	return valor
end

function general.is_absolute_path(path)
	local is_windows = package.config:sub(1, 1) == "\\"

	if is_windows then
		-- En Windows, un path absoluto puede empezar con "C:\"
		return path:match("^[A-Za-z]:\\") ~= nil
	else
		-- En Unix, un path absoluto empieza con "/"
		return path:sub(1, 1) == "/"
	end
end

-- Verificar si el path es absoluto, en caso de sí, retornar ese path.
-- en caso contrario, al ser relativo, agregar la posición desde donde
-- se está lanzado el script init.
function general.fixedRelativePathFromApp(path)
	local prefix = string.sub(path, 1, 3)
	-- WARN: Tener cuidado al hacer uso, no funcionaría si el path relativo está puesto desde
	-- el principio con .., abría que caher un artilugio
	-- TODO: Refectorizar
	if path:match("^[A-Za-z]:\\") ~= nil or path:sub(1, 1) == "/" or prefix == "..\\" or prefix == "../" then
		-- retornar path como esté
		return path
	-- retornar patj relativo fixed
	else
		local is_windows = package.config:sub(1, 1) == "\\"
		if is_windows then
			return "..\\" .. path
		else
			return "../" .. path
		end
	end
end

return general
