local httpServer = require("serverHttp")
local FileHttpHandler = require("jls.net.http.handler.FileHttpHandler")

httpServer.service():createContext("/(.*)", FileHttpHandler:new("../app/ui-app/"))
