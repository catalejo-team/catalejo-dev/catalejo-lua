local httpServer = require("serverHttp")
local HttpHandler = require("jls.net.http.HttpHandler")
local Promise = require("jls.lang.Promise")

local Path = require("jls.io.Path")
local File = require("jls.io.File")
local FileDescriptor = require("jls.io.FileDescriptor")
local json = require("jls.util.json")
local form = require("jls.net.http.form")
local general = require("general")

--
-- local workPath = Path:new("/home/johnny/projects/test/test/")
-- local configPath = Path:new(workPath, "config.json")
--
-- print(workPath)
-- print(configPath:getPathName()) -- prints 'work/config.json'
--
-- local config = {
-- 	workPath,
-- }

local config = {
	workDirPath = "", -- String del directorio actual del workspace
}

-- local dir = File:new(".")
-- local dir = File:new("/home/johnny/projects/catalejo-lua/work")
-- for _, file in ipairs(dir:listFiles()) do
-- 	if file:isFile() then
-- 		print('The file "' .. file:getPath() .. '" length is ' .. tostring(file:length()))
-- 	end
-- end

local handler = HttpHandler:new(function(_, exchange) end)

-- httpServer.service():createContext("/filesystem", function(exchange)
-- 	return handler:handle(exchange)
-- end)

local dirHandle = HttpHandler:new(function(_, exchange)
	return Promise:new(function(resolve)
		local response = exchange:getResponse()
		local query = exchange:getRequest():getTargetQuery()
		print(query)
		response:setBody("here")
		resolve()
	end)
end)

local function headersHandler(exchange)
	local response = exchange:getResponse()
	-- content-type: multipart/form-data
	response:setHeader("Content-Type", " application/json")
	response:setHeader("Access-Control-Allow-Origin", "*")
	response:setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS, DELETE")
	response:setHeader("Access-Control-Allow-Headers", "Content-Type")
	return {}
end

-- tipo de respuestas enviadas al cliente
local STATUS = {
	OK = 0,
	FAILED = 1,
}

-- Select path for work directory and get files in this directory
local dirconfigHandler = HttpHandler:new(function(_, exchange)
	if exchange:getRequestMethod() == "OPTIONS" then
		return headersHandler(exchange)
	else
		return exchange:getRequest():json():next(function(res)
			-- TODO: es posible que no se requiera la variable
			-- global config ya que los paths son entregados
			-- desde el entorno
			config.workDirPath = general.fixedRelativePathFromApp(res.workdir)
			local directory = File:new(config.workDirPath)
			local filePaths = {}
			local i = 1
			for _, file in ipairs(directory:listFiles()) do
				if file:isFile() then
					filePaths[i] = file:getPath()
					i = i + 1
				end
			end
			headersHandler(exchange)
			-- Return ok plus file paths
			return { status = STATUS.OK, data = filePaths }
		end)
	end

	-- return exchange:getRequest():text():next(function(res)
	-- return Promise:new(function(resolve)
	-- 	-- local pathDir = exchange:getRequest():getBody()
	-- 	-- local pathDir = "/home/johnny/projects/catalejo-lua/work"
	-- 	-- config["workdir"] = File:new(pathDir)
	-- 	local pathDir = Promise.await(exchange:getRequest():text())
	-- 	-- print(pathDir)
	-- 	-- config["wordir"] = File:new(res)
	-- 	-- resolve(config.workdir:getPath())
	-- 	resolve(pathDir)
	-- end)
end)

local listFilesHandler = HttpHandler:new(function(_, _)
	return Promise:new(function(resolve)
		local directory = File:new(config.workDirPath)
		local files = ""
		for _, file in ipairs(directory:listFiles()) do
			if file:isFile() then
				files = files .. file:getPath() .. ", " .. tostring(file:length()) .. "\n"
			end
		end
		resolve(files)
	end)
end)

local deleteFileHandler = HttpHandler:new(function(_, exchange)
	if exchange:getRequestMethod() == "OPTIONS" then
		return headersHandler(exchange)
	else
		return exchange:getRequest():json():next(function(body)
			local file = File:new(body.path)
			local result = file:delete()
			headersHandler(exchange)
			if result then
				return { status = STATUS.OK }
			else
				return { status = STATUS.FAILED }
			end
		end)
	end
end)

local readFileHandler = HttpHandler:new(function(_, exchange)
	if exchange:getRequestMethod() == "OPTIONS" then
		return headersHandler(exchange)
	else
		local file
		return exchange
			:getRequest()
			:json()
			:next(function(body)
				file = File:new(body.file)
			end)
			:next(function()
				return FileDescriptor.open(file:getPath(), "r")
			end)
			:next(function(fileDesc)
				return fileDesc:read(file:length()):next(function(data)
					fileDesc:close()
					headersHandler(exchange)
					return { status = STATUS.OK, data = data }
				end)
			end)
	end
end)

local writeFileHandler = HttpHandler:new(function(_, exchange)
	if exchange:getRequestMethod() == "OPTIONS" then
		return headersHandler(exchange)
	else
		local file
		local content
		return exchange
			:getRequest()
			:json()
			:next(function(body)
				file = File:new(general.fixedRelativePathFromApp(body.file))
				content = body.content
			end)
			:next(function()
				return FileDescriptor.open(file:getPath(), "w")
			end)
			:next(function(fileDesc)
				return fileDesc:write(content, 0):next(function(data)
					fileDesc:close()
					headersHandler(exchange)
					return { status = STATUS.OK, data = data }
					-- return result(exchange, { result = data })
				end)
			end)
	end
end)

local function getHeaderValueName(cadena, name)
	-- Crear una expresión regular para encontrar el campo específico, permitiendo caracteres especiales y secuencias de escape
	local patron = name .. '="([^"]-)"'
	-- Buscar el valor usando la expresión regular
	local valor = string.match(cadena, patron)
	-- Si se encuentra el valor, reemplazar las secuencias de escape
	if valor then
		valor = valor:gsub('\\"', '"') -- Reemplazar \" con "
		valor = valor:gsub("\\\\", "\\") -- Reemplazar \\ con \
	end
	return valor
end

local uploadFileHandler = HttpHandler:new(function(_, exchange)
	if exchange:getRequestMethod() == "OPTIONS" then
		return headersHandler(exchange)
	else
		local file -- Objeto que representa el archivo a crear
		local fileName, fileContent, filePath
		return exchange
			:getRequest()
			:text()
			:next(function(_)
				-- Como es un mensaje formateado por el usuario, se requiere hacer uso del parse
				local parsedMessage = form.parseFormRequest(exchange:getRequest())
				-- Se recorre el mensaje parseado y se pone los valores específicos
				for _, formData in ipairs(parsedMessage) do
					if getHeaderValueName(formData.headers["content-disposition"], "name") == "file" then
						fileName = getHeaderValueName(formData.headers["content-disposition"], "filename")
						fileContent = formData:getBody()
					elseif getHeaderValueName(formData.headers["content-disposition"], "name") == "path" then
						filePath = formData:getBody()
					end
				end
				-- En el caso de ser entregado el path desde la el request
				-- WARN: Esta opción debe ser pensada y en el caso posible ser descartada por convertirse
				-- en un problema de seguridad: no debe ser posible editar archivos que estén por fuera
				-- del área de trabajo.
				if filePath then
					file = File:new(general.join_path_and_file(filePath, fileName))
				else
					file = File:new(general.join_path_and_file(config.workDirPath, fileName))
				end
			end)
			:next(function()
				return FileDescriptor.open(file:getPath(), "w")
			end)
			:next(function(fileDesc)
				return fileDesc:write(fileContent, 0):next(function(data)
					fileDesc:close()
					headersHandler(exchange)
					return { status = STATUS.OK, data = data }
				end)
			end)
		-- general.printTable(parsedMessage, 3)
	end
end)

httpServer.service():createContext(
	"/fs/(.*)",
	HttpHandler.router({
		["dirconfig?method=PUT,OPTIONS"] = function(exchange)
			return dirconfigHandler:handle(exchange)
		end,
		["listFiles?method=GET"] = function(exchange)
			return listFilesHandler:handle(exchange)
		end,
		["readFile?method=PUT,OPTIONS"] = function(exchange)
			return readFileHandler:handle(exchange)
		end,
		["writeFile?method=PUT,OPTIONS"] = function(exchange)
			return writeFileHandler:handle(exchange)
		end,
		["deleteFile?method=PUT,OPTIONS"] = function(exchange)
			return deleteFileHandler:handle(exchange)
		end,
		["uploadFile?method=POST,OPTIONS"] = function(exchange)
			return uploadFileHandler:handle(exchange)
		end,
	})
)

print("START filesystemHandler")

-- local fsapi = {}
-- httpServer.service():createContext(
-- 	"/fs/(.*)",
-- 	HttpHandler.router({
-- 		fsapi = {
-- 			[""] = function(exchange)
-- 				return fsapi
-- 			end,
-- 			["hello(mypath)?method=GET,PUT"] = function(exchange)
-- 				return dirHandle:handle(exchange)
-- 				-- return dirHandle:handle(exchange)
-- 				-- return dirHandle:handle(exchange)
-- 				-- local response = exchange:getResponse()
-- 				-- response:setBody("hello")
-- 			end,
-- 		},
-- 	})
-- )
