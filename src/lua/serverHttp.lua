local HttpServer = require("jls.net.http.HttpServer")

HttpServer = HttpServer:new()
HttpServer:bind("::", 8080)
print("START HttpServer")

-- INFO: httpserver as module
local httpServer = {}

function httpServer.service()
	return HttpServer
end

return httpServer
