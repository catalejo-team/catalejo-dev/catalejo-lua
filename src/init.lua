-- INFO: Add paths here
-- https://www.lua.org/pil/8.1.html
local dirApp = ";../src"
package.cpath = package.cpath .. ";./lib/?.so"
package.path = package.path .. ";bin/?.lua"
package.path = package.path .. dirApp .. "/?.lua" -- ubicación del init.lua
package.path = package.path .. dirApp .. "/lua/?.lua" -- ubicación del init.lua
package.path = package.path .. dirApp .. "/ui-app/plugins/?.lua" -- ubicación del init.lua
-- package.path = package.path .. ";../app/ui-app/plugins/?.lua" -- ubicación del init.lua
-- package.path = package.path .. ";../" .. dirApp .. "/lua/?.lua" -- ubicación de dependencias

require("serverHttp")
require("filesystemHandler")
require("webServerApp")
require("plugins")
require("webviewApp")

local event = require("jls.lang.event")
event:loop()
event:close()
