PLUGINS?=nodemcu micropython-esp32

help: ## Prints help for targets with comments
	@cat $(MAKEFILE_LIST) | grep -E '^[a-zA-Z_-]+:.*?## .*$$' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'
	@echo ""
	@echo "########################################################################################"
	@echo "### CONSTRUCCIÓN DE LUAJLS EN LINUX"
	@echo "########################################################################################"
	@echo "make luaclibs-download # 1. Descargar las fuentes de luaclibs"
	@echo "make luaclibs-dependencies # 2. Instalar dependencias, en caso de fallar ir al pasa 3.2"
	@echo "make luaclibs-linux-build # 3.1 Construir luajls e ir al paso 4, si FALLA ir a paso 3.2"
	@echo "# 3.2 en el caso de fallar el paso 2 o 3.1 siga los pasos 3.2.1 y 3.2.2"
	@echo "make docker-image-create # 3.2.1 Se crea la imagen de docker para compiliar el luajls"
	@echo "make docker-luaclibs-linux-build # 3.2.2 Compilar luajls"
	@echo "make luaclibs-linux-install # 4. Instalar luajls con lua-libserialport en bin/"
	@echo "# 5. Ha finalizado con éxito"
	@echo "########################################################################################"
	@echo "### CONSTRUCCIÓN DE CATALEJO EDITOR EN LINUX"
	@echo "########################################################################################"
	@echo "make dist-linux <-plugins> # Construirá una versión del editor sin/con plugins"
	@echo "########################################################################################"
	@echo "### PLUGINS, DESCARGA E INSTALACIÓN"
	@echo "########################################################################################"
	@echo "make plugins-install PLUGINS=\"listar plugins separados por espacio\""
	@echo "# Instalar plugins en el editor antes de crear la dist del editor"
	@echo "Plugins disponibles: $(PLUGINS)"
	@echo "########################################################################################"
	@echo "### DECARGA DE LUAJLS PARA WINDOWS (PRECOMPILADO)"
	@echo "########################################################################################"
	@echo "make luajls-w64-download # Descarga de precompilado de luajls para w64 y libserial.dll"
	@echo "########################################################################################"
	@echo "### CONSTRUCCIÓN DE CATALEJO EDITOR PARA WINDOWS"
	@echo "########################################################################################"
	@echo "make dist-w64<-plugins> # Construirá una version del editor sin/con plugins"
	@echo "########################################################################################"
	@echo "### CONSTRUCCIÓN DEL FRONTEND DE CATALEJO EDITOR PARA ANDROID"
	@echo "########################################################################################"
	@echo "make dist-android <-plugins> # Construirá una version del frontend sin/con plugins"
	@echo "Requiere https://gitlab.com/catalejo-team/catalejo-dev/catalejo-editor-mobile-v2.git"
	@echo "Copiar ./dist/catalejo_android.zip en app/src/main/res/raw/ del git y construir"

d: dev

app:	## Lanzar el modo de distibución de esta aplicación
	cd bin/ && ./lua ../app/init.lua

app-linux-dependencies: ## Dependencias webview para la app en linux
	sudo apt install libsvtav1enc1 libwebkit2gtk-4.0-37

dev: ## Lanzar el modo de desarrollo de catalejo-lua
	@echo "Lanzar la aplicación en modo desarrollo"
	cd bin/ && ./lua ../src/init.lua

luaclibs-dependencies: ## Instalar dependencias con apt de luaclibs
	@echo "Dependencias para la compilación de luaclibs, en el caso de fallar, deberá construir con docker"
	bash ./scripts/luajls.bash dependencies-linux

luaclibs-download: ## Decargar los archivos fuente de luajlclibs, hacerlo una única vez, este proceso suele tardar bastantes minutos
	@echo "Descargando luaclibs"
	bash ./scripts/luajls.bash download
	bash ./scripts/luajls.bash checkout
	@echo "Este proceso se TARDA BASTANTES minutos, se recomienda NO BORRAR estas fuentes en el proceso de desarrollo"

luaclibs-linux-build:	## Construir luajls para Linux
	@echo "Construcción de luajls desde luaclibs"
	bash ./scripts/luajls.bash build-linux

luaclibs-linux-install: ## Instalar luajls en bin/
	@echo "Construcción de luajls desde luaclibs"
	bash ./scripts/luajls.bash install-linux

##########################################################
## BUILD luajls for Linux from Docker image
##########################################################
DOCKER_IMG=debian-docker-luajls-builder
PATH_DOCKERFILE=./scripts/luajls-builder-dockerfile/

# Docker image foir build linux luajls
docker-image-create: ## Crear la imagen docker del compilador con podman
	# Make image
	cd $(PATH_DOCKERFILE) \
		&& podman build -t $(DOCKER_IMG) .

docker-luaclibs-linux-build: ## Construir luajls para linux desde el contenedor
	podman run --rm -it \
		-v `pwd`:/build-app/:Z \
		-w /build-app \
		--user $(id -u):$(id -g) \
		$(DOCKER_IMG) \
		make luaclibs-linux-build

docker-help: ## Ayudas sobre cómo usar PODMAN (docker).
	@echo "#### HELP PODMAN-DOCKER ####"
	@echo "sudo apt install podman # Instalar podman"
	@echo "## MANEJO DE CONTENEDORES ##"
	@echo "podman ps -a # Listar contenedores"
	@echo "podman stop <container-id> # Detener contenedores (si está en ejecución)"
	@echo "podman rm <container-id> # Eliminar un contenedor"
	@echo "podman rm \$$(podman ps -a -q) # Eliminar todos los contenedores detenidos"
	@echo "## MANEJO DE IMÁGENES ##"
	@echo "podman images # Listar imagenes"
	@echo "podman rmi <image-id> # Eleminar una imagen específica"
	@echo "podman image prune # Eliminiar todas las imágenes no usadas"
##########################################################

build-backend: ## Crear de la app el backend (Lua scripts de luajls)
	bash ./scripts/build-editor.bash install_backend

build-frontend: ## Crear de la app el frontend (vue)
	bash ./scripts/build-editor.bash install_frontend

build-app: build-backend build-frontend ## Crear la app de distribución completa

dist-remove:
	rm -rf app

##########################################################
# PLUGINS, DESCARGA E INSTALACIÓN
##########################################################
plugins-install: ## Descarga e instalación de plugins para catalejo editor
	bash ./scripts/catalejo-plugins/catalejo-plugins.bash plugins_download "$(PLUGINS)"
	bash ./scripts/catalejo-plugins/catalejo-plugins.bash plugins_install "$(PLUGINS)"
	bash ./scripts/catalejo-plugins/catalejo-plugins.bash config_plugins_js "$(PLUGINS)"
	bash ./scripts/catalejo-plugins/catalejo-plugins.bash config_plugins_lua "$(PLUGINS)"

##########################################################
# Creación de distribución de catalejo para linux
##########################################################
dist-linux: dist-remove build-app
	bash ./scripts/build-editor.bash dist_linux
dist-linux-plugins: dist-remove build-app plugins-install ## Crear distribución Linux con plugins
	bash ./scripts/build-editor.bash dist_linux
##########################################################
# Creación de distribución de catalejo para android
##########################################################
dist-android: dist-remove build-app
	bash ./scripts/build-editor.bash dist_android
dist-android-plugins: dist-remove build-app plugins-install ## Crear frontend para Android con plugins
	bash ./scripts/build-editor.bash dist_android
##########################################################
# Creación de distribución de catalejo para windows 64
##########################################################
luajls-w64-download: ## Descargar lualjs y libserial para w64
	bash ./scripts/luajls.bash download-w64
dist-w64: dist-remove build-app
	bash ./scripts/build-editor.bash dist_w64
dist-w64-plugins: dist-remove build-app plugins-install ## Crear distribución Windows con plugins
	bash ./scripts/build-editor.bash dist_w64
##########################################################

bruno-install:	## Descarga de bruno, herramienta para probar los request http
	@echo "Instalación de la aplicación bruno, la cual es un cliente para las pruebas del backen (http)."
	bash ./scripts/bruno.bash install

bruno:	## Lanzar el cliente bruno
	@echo "Lanzar la aplicación para las diferentes pruebas del backend."
	./build-apps/bruno/bruno &

clean: ## Limpiar app/*
	rm -rf app/*

.PHONY: app
